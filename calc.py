from tkinter import *
    
window = Tk()

w = 9
h = 3

a = 0
b = 0

operation_pending = False

def type_digit(digit):
    global operation_pending
    if operation_pending:
        entr.delete(0, END)
        operation_pending = False
    entr.insert(END, digit)

def type_operator(operator):
    global operation_pending
    lbl.configure(text = entr.get())
    operation_pending = True
    op.configure(text = operator)

def click_b1():
    type_digit(1)
def click_b2():
    type_digit(2)
def click_b3():
    type_digit(3)
def click_b4():
    type_digit(4)
def click_b5():
    type_digit(5)
def click_b6():
    type_digit(6)
def click_b7():
    type_digit(7)
def click_b8():
    type_digit(8)
def click_b9():
    type_digit(9)
def click_b0():
    type_digit(0)
def click_clr():
    entr.delete(0, END)
    lbl.configure(text = '')
    op.configure(text = '')
def click_plus():
    type_operator('+')
def click_minus():
    type_operator('-')
def click_multiply():
    type_operator('×')
def click_divide():
    type_operator('÷')
def click_equal():
    operand1 = lbl['text']
    operator = op['text']
    operand2 = entr.get()
    
    if operator not in ['+', '-', '×', '÷']:
        return
    elif not all(map(str.isdigit, operand1)) or not all(map(str.isdigit, operand2)):
        result = 'invalid operands'
    else:
        if operator == '+':
            result = int(operand1) + int(operand2)
        if operator == '-':
            result = int(operand1) - int(operand2)
        if operator == '×':
            result = int(operand1) * int(operand2)
        if operator == '÷':
            if int(operand2) != 0:
                result = int(operand1) / int(operand2)
            elif int(operand2) == 0 and int(operand1) != 0:
                result = '∞'
            else:
                result = 'undefined'
    entr.delete(0, END)
    lbl.configure(text = '')
    op.configure(text = '')
    entr.insert(END, result)

lbl = Label()
op = Label()
entr = Entry(window, width = 30, font = '"Courier New" 22')
btn1 = Button(window, text = '1', width = w, height = h, command = click_b1)
btn2 = Button(window, text = '2', width = w, height = h, command = click_b2)
btn3 = Button(window, text = '3', width = w, height = h, command = click_b3)
btn4 = Button(window, text = '4', width = w, height = h, command = click_b4)
btn5 = Button(window, text = '5', width = w, height = h, command = click_b5)
btn6 = Button(window, text = '6', width = w, height = h, command = click_b6)
btn7 = Button(window, text = '7', width = w, height = h, command = click_b7)
btn8 = Button(window, text = '8', width = w, height = h, command = click_b8)
btn9 = Button(window, text = '9', width = w, height = h, command = click_b9)
btn0 = Button(window, text = '0', width = w, height = h, command = click_b0)
btn_clr = Button(window, text = 'C', width = w, height = h, fg = 'red', command = click_clr)
btn_plus = Button(window, text = '+', width = w, height = h, command = click_plus)
btn_minus = Button(window, text = '-', width = w, height = h, command = click_minus)
btn_multiply = Button(window, text = '×', width = w, height = h, command = click_multiply)
btn_divide = Button(window, text = '÷', width = w, height = h, command = click_divide)
btn_equal = Button(window, text = '=', width = w, height = 4*h, command = click_equal)

lbl.grid(column = 1, row = 0, columnspan = 2)
op.grid(column = 2, row = 0, columnspan = 2)
entr.grid(column = 1, row = 1, columnspan = 5)
btn1.grid(column = 1, row = 2)
btn2.grid(column = 2, row = 2)
btn3.grid(column = 3, row = 2)
btn4.grid(column = 1, row = 3)
btn5.grid(column = 2, row = 3)
btn6.grid(column = 3, row = 3)
btn7.grid(column = 1, row = 4)
btn8.grid(column = 2, row = 4)
btn9.grid(column = 3, row = 4)
btn0.grid(column = 2, row = 5)
btn_clr.grid(column = 3, row = 5)
btn_plus.grid(column = 4, row = 5) 
btn_minus.grid(column = 4, row = 4)
btn_multiply.grid(column = 4, row = 3)
btn_divide.grid(column = 4, row = 2)
btn_equal.grid(column = 5, row = 2, rowspan = 4)

window.mainloop()